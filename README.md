# Bookshelf Application #

A sample spring boot application with two types of entities: *books* and *authors*. The model is such that each book can be written by a number of co-authors. In turn, an author may have written multiple books. For simplicity, an author has one business field which is the author's name. Properties of a book: the title (limited to 255 characters), the abstract (limited to 1024 characters) and the list of co-authors (max of 255 characters for the name field of each co-author):
```
{
    "title": "Introduction to algorithms",
    "abstract": "...uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth...",
    "authors": ["Thomas H. Cormen", "Charles E. Leiserson", "Ronald L. Rivest", "Clifford Stein"]
}
```


### Relational model ###
Authors and books are mapped using the following rdbms schema:

#### authors table: ####
```
 Field   | Type         | Null | Key     | Default 
 -------- -------------- ------ --------- ---------
 id      | varchar(255) | NO   | Primary | NULL    
 name    | varchar(255) | NO   | Unique  | NULL    
 version | bigint(20)   | YES  |         | NULL    
```
#### books table: ####
```
 Field       | Type          | Null | Key     | Default 
------------- --------------- ------ --------- --------- 
 id          | varchar(255)  | NO   | Primary | NULL 
 description | varchar(1024) | YES  |         | NULL 
 title       | varchar(255)  | NO   | Unique  | NULL 
 version     | bigint(20)    | YES  |         | NULL  
```
#### author_book join table: ####
```
 Field     | Type         | Null | Key     | Default
----------- -------------- ------ --------- --------
 id        | varchar(255) | NO   | Primary | NULL  
 version   | bigint(20)   | YES  |         | NULL   
 author_id | varchar(255) | NO   | FK      | NULL   
 book_id   | varchar(255) | NO   | FK      | NULL  
```
### Public APIs ###
The application exposes the following http endpoints:

* #####register a book#####
    Registers a new book into the system. Responds with a URL of the created resource.

    __http method__=POST __request uri__=/api/v1/books __Content-Type__=application/json __Accept__=application/json

    
```
    Sample request payload:
    
    {
        "title": "Introduction to algorithms",
        "abstract": "...uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth...",
        "authors": ["Thomas H. Cormen", "Charles E. Leiserson", "Ronald L. Rivest", "Clifford Stein"]
    }
    
    Sample response:
    
    {
        "href": "http://server:port/api/v1/books/123e4567-e89b-12d3-a456-426655440000"
    }
```
* #####search for a book by id#####
    Retrieves a book information by its id, which can be obtained during the register flow.

    __http method__=GET __request uri__=/api/v1/books/{id} __Accept__=application/json

    
```
    Sample response:
    
    {
        "title": "Introduction to algorithms",
        "abstract": "...uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth...",
        "authors": ["Thomas H. Cormen", "Charles E. Leiserson", "Ronald L. Rivest", "Clifford Stein"]
    }
```
* #####search for a book by title#####
    Retrieves a book information by its title, specified as a query parameter.

    __http method__=GET __request uri__=/api/v1/books?title=_yourBookTitle_ __Accept__=application/json

    
```
    Sample response:
    
    {
        "title": "Introduction to algorithms",
        "abstract": "...uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth...",
        "authors": ["Thomas H. Cormen", "Charles E. Leiserson", "Ronald L. Rivest", "Clifford Stein"]
    }
```
* #####list author's books#####
    Lists books of an author, specified as a query parameter.

    __http method__=GET __request uri__=/api/v1/books?author=_yourAuthor_ __Accept__=application/json

    
```
    Sample response:
    
    {
        "books": ["Alice's Adventures in Wonderland", "Through the Looking-Glass, and What Alice Found There"]
    }
```

### How do I get set up? ###

* #####build#####

    Clone project into your local directory, change to the project root and execute `./gradlew build`

* #####running the application#####

    The application may be configured to run with either h2 or MySql databases. **Note:** when choosing to run with MySql, make sure you have your db server with the dedicated database properly set in advance.

    - In order to run with h2, locate the executable jar under `/build/libs` and execute `java -Dspring.profiles.active=integration -jar bookshelf-1.0.jar`

    - In order to run with MySql, locate the executable jar under `/build/libs` and execute `java -jar bookshelf-1.0.jar`

* #####custom configuration#####

    The required configuration properties may be customized directly in `/bookshelf/src/main/resources/application.yml`, or through passing jvm parameters on starting the application. For instance, to start with db user = admin, password = secret and database = bookshelf, and having your db server running on 172.10.0.87 and listening on port 12345, execute `java -Ddb.user=admin -Ddb.password=secret -Ddb.url=jdbc:mysql://172.10.0.87:12345/bookshelf -jar bookshelf-1.0.jar`