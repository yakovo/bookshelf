package org.bookshelf.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bookshelf.model.Book;
import org.bookshelf.model.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/books")
@Validated
class BookResource extends FaultHandler {

    static class Ref {
        @JsonProperty
        String href;

        Ref() {
            super();
        }

        Ref(String href) {
            this.href = href;
        }
    }

    static class Books {
        @JsonProperty
        List<String> books;

        Books() {
            super();
        }

        Books(List<String> books) {
            this.books = books;
        }
    }

    private final BookService service;

    public BookResource(BookService service) {
        this.service = service;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/{id}")
    public Book find(@PathVariable UUID id) {
        return service.find(id.toString());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> find(@RequestParam(value = "title", required = false) @Size(min = 1) String title, @RequestParam(value = "author", required = false) @Size(min = 1) String author) {

        if (author == null) {
            return new ResponseEntity<>(service.findByTitle(title), HttpStatus.OK);
        }
        if (title == null) {
            return new ResponseEntity<>(new Books(service.findByAuthor(author)), HttpStatus.OK);
        }
        throw new UnsupportedOperationException("search by both author and book title is not supported");
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Ref register(@RequestBody @Valid Book book, HttpServletRequest request) {
        return new Ref(request.getRequestURL() + "/" + service.register(book));
    }
}