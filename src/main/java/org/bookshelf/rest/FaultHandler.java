package org.bookshelf.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bookshelf.model.ResourceAlreadyExistsException;
import org.bookshelf.model.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.util.function.BiFunction;

class FaultHandler {

    private final static Logger logger = LoggerFactory.getLogger(FaultHandler.class);

    private final static BiFunction<Throwable, HttpStatus, ResponseEntity<Fault>> handler = (e, stat) -> {
        logger.error(e.getMessage(), e);
        return new ResponseEntity<>(new Fault(e.getMessage()), stat);
    };

    static class Fault {
        @JsonProperty
        String message;

        Fault() {
            super();
        }

        Fault(String message) {
            this.message = message;
        }
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<Fault> handle(Throwable e) {
        return handler.apply(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Fault> handle(ResourceNotFoundException e) {
        return handler.apply(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<Fault> handle(ResourceAlreadyExistsException e) {
        return handler.apply(e, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Fault> handle(ConstraintViolationException e) {
        return handler.apply(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    public ResponseEntity<Fault> handle(UnsupportedOperationException e) {
        return handler.apply(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Fault> handle(MethodArgumentTypeMismatchException e) {
        return handler.apply(e.getMostSpecificCause(), HttpStatus.BAD_REQUEST);
    }
}