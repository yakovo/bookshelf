package org.bookshelf.model;

public class ResourceAlreadyExistsException extends ResourceException {

    private final static String MESSAGE_TPL = "the specified resource %s with property %s and value %s already exists";

    @Override
    public String getMessage() {
        return String.format(MESSAGE_TPL, getResourceType(), getResourceField(), getFieldValue());
    }

    ResourceAlreadyExistsException(String resourceType, String resourceField, String fieldValue) {
        super(resourceType, resourceField, fieldValue);
    }
}