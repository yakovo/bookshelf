package org.bookshelf.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Book {

    @JsonProperty
    @NotBlank
    private final String title;

    @JsonProperty("abstract")
    private final String description;

    @JsonProperty
    @NotEmpty
    private final String[] authors;

    Book() {
        this(null, null);
    }

    private Book(String title, String description, String... authors) {
        this.title = title;
        this.description = description;
        this.authors = authors;
    }

    Book withTitle(String title) {
        return new Book(title, description, authors);
    }

    Book withDescription(String description) {
        return new Book(title, description, authors);
    }

    Book addAuthors(String... authors) {
        HashSet<String> union = new HashSet<>(Arrays.asList(authors));
        union.addAll(getAuthors());
        return new Book(title, description, union.toArray(new String[]{}));
    }

    String getTitle() {
        return title;
    }

    String getDescription() {
        return description;
    }

    List<String> getAuthors() {
        return Arrays.asList(authors);
    }
}