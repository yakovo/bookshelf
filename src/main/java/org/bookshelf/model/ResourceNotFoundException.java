package org.bookshelf.model;

public class ResourceNotFoundException extends ResourceException {

    private final static String MESSAGE_TPL = "the specified resource %s with property %s and value %s does not exist";

    @Override
    public String getMessage() {
        return String.format(MESSAGE_TPL, getResourceType(), getResourceField(), getFieldValue());
    }

    ResourceNotFoundException(String resourceType, String resourceField, String fieldValue) {
        super(resourceType, resourceField, fieldValue);
    }
}