package org.bookshelf.model;

import org.bookshelf.dao.AuthorBookRepository;
import org.bookshelf.dao.AuthorRepository;
import org.bookshelf.dao.BookRepository;
import org.bookshelf.domain.AuthorBook;
import org.bookshelf.domain.AuthorEntity;
import org.bookshelf.domain.BookEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final static BiFunction<Book, AuthorBook, Book> appendAuthor = (b, ab) -> b.addAuthors(ab.getAuthor().getName());

    private final static BinaryOperator<Book> mergeBook = (b1, b2) -> b1.addAuthors(b2.getAuthors().toArray(new String[]{}));

    private final AuthorBookRepository authorBooks;

    private final BookRepository books;

    private final AuthorRepository authors;

    public BookService(AuthorBookRepository authorBooks, BookRepository books, AuthorRepository authors) {
        this.authorBooks = authorBooks;
        this.books = books;
        this.authors = authors;
    }

    private Book findByBook(BookEntity book) {
        return authorBooks.findByBook(book).stream().reduce(//reduce found author<->book associations into a book
                new Book().withTitle(book.getTitle()).withDescription(book.getDescription()),//feed reduce with the initial value
                appendAuthor,//append authors
                mergeBook);//merge
    }

    /**
     * Returns a book by the specified id.
     *
     * @param id - uniquely identifies a book in the system
     * @return a book with the specified id.
     * @throws ResourceNotFoundException if the specified book does not exist
     */
    @Transactional(readOnly = true, timeout = 3)
    public Book find(String id) throws ResourceNotFoundException {
        return findByBook(books.findById(id).orElseThrow(() -> new ResourceNotFoundException("book", "id", id)));
    }

    /**
     * Retrieves all books, written by the specified author.
     *
     * @param name - specifies a name of the author
     * @return all books, written by the specified author.
     * @throws ResourceNotFoundException if the specified author does not exist
     */
    @Transactional(readOnly = true, timeout = 5)
    public List<String> findByAuthor(String name) throws ResourceNotFoundException {
        AuthorEntity author = authors.findByName(name).orElseThrow(() -> new ResourceNotFoundException("author", "name", name));
        return authorBooks.findByAuthor(author).stream().map(authorBook -> authorBook.getBook().getTitle()).collect(Collectors.toList());
    }

    /**
     * Returns a book by the specified title.
     *
     * @param title - specifies a title of the book
     * @return a book with the specified title.
     * @throws ResourceNotFoundException if the specified book does not exist
     */
    @Transactional(readOnly = true, timeout = 3)
    public Book findByTitle(String title) throws ResourceNotFoundException {
        return findByBook(books.findByTitle(title).orElseThrow(() -> new ResourceNotFoundException("book", "title", title)));
    }

    /**
     * Registers a new book into the system.
     *
     * @param book - specifies a book
     * @return a unique identifier of the created book
     * @throws ResourceAlreadyExistsException if a book with the specified name already exists
     */
    @Transactional(timeout = 5)
    public String register(Book book) throws ResourceAlreadyExistsException {

        if (books.findByTitle(book.getTitle()).isPresent()) {//same book cannot be registered twice
            throw new ResourceAlreadyExistsException("book", "title", book.getTitle());
        }
        BookEntity entity = books.saveAndFlush(new BookEntity(book.getTitle(), book.getDescription()));
        book.getAuthors().forEach(authorName -> {
            Optional<AuthorEntity> author = authors.findByName(authorName);

            if (!author.isPresent()) {//author is not present - create a new one
                author = Optional.of(authors.saveAndFlush(new AuthorEntity(authorName)));
            }
            authorBooks.save(new AuthorBook(entity, author.get()));
        });
        return entity.getId();
    }
}