package org.bookshelf.model;

abstract class ResourceException extends RuntimeException {

    private final String resourceType;

    private final String resourceField;

    private final String fieldValue;

    public abstract String getMessage();

    ResourceException(String resourceType, String resourceField, String fieldValue) {
        this(resourceType, resourceField, fieldValue, null);
    }

    ResourceException(String resourceType, String resourceField, String fieldValue, Throwable cause) {
        super(cause);
        this.fieldValue = fieldValue;
        this.resourceField = resourceField;
        this.resourceType = resourceType;
    }

    String getResourceType() {
        return resourceType;
    }

    String getResourceField() {
        return resourceField;
    }

    String getFieldValue() {
        return fieldValue;
    }
}