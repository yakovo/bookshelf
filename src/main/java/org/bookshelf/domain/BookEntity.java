package org.bookshelf.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "books")
@Access(AccessType.FIELD)
public class BookEntity implements Serializable {

    @Id
    private final String id;

    @Version
    private Long version; //enable optimistic lock

    @Column(nullable = false, unique = true)
    @NotBlank
    private final String title;

    @Column(length = 1024)
    private final String description;

    BookEntity() {
        this(null, null);
    }

    public BookEntity(String title, String description) {
        this.title = title;
        this.description = description;
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}