package org.bookshelf.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "events")
@Access(AccessType.FIELD)
public class EventEntity implements Serializable {

    @Id
    private final String id;

    @Column(nullable = false)
    private final byte[] event;

    EventEntity() {
        this(new byte[0]);
    }

    public EventEntity(byte[] event) {
        this.event = event;
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public byte[] getEvent() {
        return event;
    }
}