package org.bookshelf.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "author_book",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"author_id", "book_id"})
        })
@Access(AccessType.FIELD)
public class AuthorBook implements Serializable {

    @Id
    private final String id;

    @Version
    private Long version; //enable optimistic lock

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private final BookEntity book;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private final AuthorEntity author;

    AuthorBook() {
        this(null, null);
    }

    public AuthorBook(BookEntity book, AuthorEntity author) {
        id = UUID.randomUUID().toString();
        this.author = author;
        this.book = book;
    }

    public BookEntity getBook() {
        return book;
    }

    public AuthorEntity getAuthor() {
        return author;
    }
}