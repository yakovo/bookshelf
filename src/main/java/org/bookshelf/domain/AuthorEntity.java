package org.bookshelf.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "authors")
@Access(AccessType.FIELD)
public class AuthorEntity implements Serializable {

    @Id
    private final String id;

    @Version
    private Long version; //enable optimistic lock

    @Column(nullable = false, unique = true)
    @NotBlank
    private final String name;

    AuthorEntity() {
        this(null);
    }

    public AuthorEntity(String name) {
        this.name = name;
        id = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }
}