package org.bookshelf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@SpringBootApplication
public class BookShelfApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookShelfApplication.class, args);
    }

    @Resource
    Properties jpaProperties;

    @Autowired
    DataSource dataSource;

    @EnableConfigurationProperties({DbSettings.class})
    public static class DefaultConfig {

        @Autowired
        DbSettings dbSettings;

        @Bean(destroyMethod = "close")
        public DataSource dataSource() {
            org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
            ds.setUsername(dbSettings.getUser());
            ds.setPassword(dbSettings.getPassword());
            ds.setDriverClassName(dbSettings.getDriver());
            ds.setInitialSize(dbSettings.getPool().getInitialSize());
            ds.setMaxIdle(dbSettings.getPool().getMaxIdle());
            ds.setMaxActive(dbSettings.getPool().getMaxActive());
            ds.setMinIdle(dbSettings.getPool().getMinIdle());
            ds.setUrl(dbSettings.getUrl());
            return ds;
        }

        @Bean
        public Properties jpaProperties() {
            Properties props = new Properties();
            props.setProperty("hibernate.dialect", dbSettings.getJpa().getDialect());
            props.setProperty("hibernate.show_sql", Boolean.toString(dbSettings.getJpa().getShowSql()));
            props.setProperty("hibernate.format_sql", Boolean.toString(dbSettings.getJpa().getFormatSql()));
            props.setProperty("hibernate.hbm2ddl.auto", dbSettings.getJpa().getDdl());
            return props;
        }
    }

    @Bean
    public AbstractPlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        bean.setDataSource(dataSource);
        bean.setJpaProperties(jpaProperties);
        bean.setPackagesToScan("org.bookshelf.domain");
        bean.setPersistenceUnitName("bs");
        return bean;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor bean = new MethodValidationPostProcessor();
        bean.setValidator(validator());
        return bean;
    }
}