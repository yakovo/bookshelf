package org.bookshelf.dao;

import org.bookshelf.domain.AuthorBook;
import org.bookshelf.domain.AuthorEntity;
import org.bookshelf.domain.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * DAO to access join table which keeps the author<->book associations.
 */
public interface AuthorBookRepository extends JpaRepository<AuthorBook, String> {

    /**
     * Retrieves a list of author<->book associations from the join table by the specified book.<br/>
     * No pagination is required since the number of co-authors for a specific book is limited.
     *
     * @param book - specifies a book entity
     * @return a list of author<->book associations from the join table by the specified book
     */
    List<AuthorBook> findByBook(BookEntity book);

    /**
     * Retrieves a list of author<->book associations from the join table by the specified author.<br/>
     * No pagination is required since the number of books that could be written by a single author is limited.
     *
     * @param author - specifies an author
     * @return a list of author<->book associations from the join table by the specified author
     */
    List<AuthorBook> findByAuthor(AuthorEntity author);
}