package org.bookshelf.dao;

import org.bookshelf.domain.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * DAO to access book entities.
 */
public interface BookRepository extends JpaRepository<BookEntity, String> {

    /**
     * Returns an optional of book by the specified title; if found, the returned optional can be safely realized.
     *
     * @param title - specifies a title of the book
     * @return an optional of book by the specified title; if found, the returned optional can be safely realized.
     */
    Optional<BookEntity> findByTitle(String title);

    /**
     * Returns an optional of book by the specified id; if found, the returned optional can be safely realized.
     *
     * @param id - uniquely identifies a book within a system
     * @return an optional of book by the specified id; if found, the returned optional can be safely realized.
     */
    Optional<BookEntity> findById(String id);
}