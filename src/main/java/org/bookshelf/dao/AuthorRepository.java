package org.bookshelf.dao;

import org.bookshelf.domain.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * DAO to access author entities.
 */
public interface AuthorRepository extends JpaRepository<AuthorEntity, String> {

    /**
     * Returns an optional of author by the specified name; if found, the returned optional can be safely realized.
     *
     * @param name - specifies a name of the author
     * @return an optional of author by the specified name; if found, the returned optional can be safely realized.
     */
    Optional<AuthorEntity> findByName(String name);
}