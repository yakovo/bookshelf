package org.bookshelf.rest;

import org.bookshelf.BookShelfApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookShelfApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = {"integration"})
public class BookResourceTest {

    @LocalServerPort
    private int port;

    @Test
    public void testFindBookByTitleAndNotFound() {
        given()
                .port(port)
                .basePath("/api/v1")
                .queryParam("title", "Lord of Rings")
                .get("/books")
                .then()
                .contentType(JSON)
                .statusCode(404)
                .body("message", response -> equalTo("the specified resource book with property title and value Lord of Rings does not exist"));
    }

    @Test
    public void testRegisterBook() {
        given()
                .port(port)
                .basePath("/api/v1")
                .contentType(JSON)
                .body("{\"title\": \"Introduction to Algorithms\"," +
                        "\"abstract\": \"Introduction to Algorithms uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth, yet makes their design and analysis accessible to all levels of readers. Each chapter is relatively self-contained and can be used as a unit of study. The algorithms are described in English and in a pseudocode designed to be readable by anyone who has done a little programming. The explanations have been kept elementary without sacrificing depth of coverage or mathematical rigor.\"," +
                        "\"authors\": [\"Thomas H. Cormen\", \"Charles E. Leiserson\", \"Ronald L. Rivest\", \"Clifford Stein\"]}")
                .post("/books")
                .then()
                .statusCode(200)
                .body("href", response -> containsString("/api/v1/books/"));
        given()
                .port(port)
                .basePath("/api/v1")
                .queryParam("title", "Introduction to Algorithms")
                .get("/books")
                .then()
                .contentType(JSON)
                .statusCode(200)
                .body("authors.findAll {it != null}", response -> hasItems("Thomas H. Cormen", "Charles E. Leiserson", "Ronald L. Rivest", "Clifford Stein"));
    }

    @Test
    public void testFindBooksByAuthor() {
        given()
                .port(port)
                .basePath("/api/v1")
                .contentType(JSON)
                .body("{\"title\": \"Introduction to Algorithms I\"," +
                        "\"authors\": [\"Thomas H. Cormen\", \"Charles E. Leiserson\", \"Ronald L. Rivest\", \"Clifford Stein\"]}")
                .post("/books")
                .then()
                .statusCode(200)
                .body("href", response -> containsString("/api/v1/books/"));
        given()
                .port(port)
                .basePath("/api/v1")
                .contentType(JSON)
                .body("{\"title\": \"Introduction to Algorithms II\"," +
                        "\"authors\": [\"Thomas H. Cormen\", \"Charles E. Leiserson\", \"Ronald L. Rivest\", \"Clifford Stein\"]}")
                .post("/books")
                .then()
                .statusCode(200)
                .body("href", response -> containsString("/api/v1/books/"));
        given()
                .port(port)
                .basePath("/api/v1")
                .queryParam("author", "Thomas H. Cormen")
                .get("/books")
                .then()
                .contentType(JSON)
                .statusCode(200)
                .body("books.findAll {it != null}", hasItems("Introduction to Algorithms I", "Introduction to Algorithms II"));
    }
}